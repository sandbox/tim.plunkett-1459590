<?php

/**
 * @file
 * @todo.
 */

class fullcalendar_drop_list_plugin_display_extender extends views_plugin_display_extender {

  function options_definition_alter(&$options) {
    $options['fullcalendar_drop_list'] = array('default' => array());
    $options['fullcalendar_drop_list_remove'] = array('default' => FALSE);
  }

  function options_form(&$form, &$form_state) {
    if ($form_state['section'] != 'fullcalendar_drop_list') {
      return;
    }

    $options = $this->display->get_option('fullcalendar_drop_list');

    $form['#title'] .= t('FullCalendar Drop List');

    $form['drop_list'] = array(
      '#tree' => TRUE,
      '#pre_render' => array('views_ui_pre_render_add_fieldset_markup'),
    );
    $form['drop_list']['use'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use this View as a drop list for FullCalendar?'),
      '#default_value' => $options['use'],
    );
    $form['drop_list']['field'] = array(
      '#type' => 'select',
      '#title' => t('Select the fields to use:'),
      '#options' => $this->fullcalendar_drop_list_fields(array('date', 'datetime', 'datestamp')),
      '#default_value' => $options['field'],
      '#dependency' => array('edit-drop-list-use' => array(1)),
    );
    $form['drop_list']['remove'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remove events once dropped onto the FullCalendar?'),
      '#default_value' => $options['remove'],
      '#dependency' => array('edit-drop-list-use' => array(1)),
    );
  }

  function options_submit(&$form, &$form_state) {
    if ($form_state['section'] != 'fullcalendar_drop_list') {
      return;
    }

    if (empty($form_state['values']['drop_list']['use']) || empty($form_state['values']['drop_list']['field'])) {
      $form_state['values']['drop_list']['field'] = '';
    }

    $this->display->set_option('fullcalendar_drop_list', $form_state['values']['drop_list']);
  }

  function options_summary(&$categories, &$options) {
    $option = $this->display->get_option('fullcalendar_drop_list');
    $options['fullcalendar_drop_list'] = array(
      'category' => 'other',
      'title' => t('FullCalendar Drop List'),
      'value' => $option['use'] ? t('Yes') : t('No'),
    );
  }

  function pre_execute() {
    $options = $this->display->get_option('fullcalendar_drop_list');
    // If the option is disabled, return.
    if (empty($options['use'])) {
      return;
    }

    // If the selected date field is not in the query, return.
    if (empty($this->view->field[$options['field']])) {
      return;
    }

    // Gather some metadata.
    $field = $this->view->field[$options['field']];
    $entity_type = $field->definition['entity_tables'][$field->base_table];
    $display_id = $this->view->current_display;
    $entity_info = entity_get_info($entity_type);
    $entity_key = $entity_info['entity keys']['id'];
    if (isset($entity_info['entity keys']['label'])) {
      $title_key = $entity_info['entity keys']['label'];
    }
    // User doesn't have a label key, use name.
    elseif ($entity_type == 'user') {
      $title_key = 'name';
    }
    // If we don't know the label key, return.
    else {
      return;
    }

    // Add a hidden entity_id field.
    $entity_id = $this->view->add_item($display_id, 'field', $field->base_table, $entity_key, array('exclude' => TRUE, "link_to_$entity_type" => FALSE));

    // Add a rewritten title field.
    $title_id = $this->view->add_item($display_id, 'field', $field->base_table, $title_key);
    $title = $this->view->get_item($display_id, 'field', $title_id) + array(
      'label' => '',
      "link_to_$entity_type" => FALSE,
      'alter' => array(
        'alter_text' => TRUE,
        'text' => '<span class="fullcalendar-drop-list element-hidden" eid="[' . $entity_id . ']" field="' . $options['field'] . '">[' . $title_id . ']</span>',
      ),
    );
    $this->view->set_item($display_id, 'field', $title_id, $title);

    // Reset the field handlers.
    // @todo Remove this workaround.
    $this->display->handlers['field'] = NULL;
    $this->view->inited = NULL;
    $this->view->init_handlers();

    // Add these settings to JS.
    $settings = array(
      'dom_ids' => array(
        'view-dom-id-' . $this->view->dom_id,
      ),
      'field' => $field->field,
      'entity_type' => $entity_type,
      'remove' => (bool) $options['remove'],
    );

    ctools_add_js('fullcalendar_drop_list', 'fullcalendar_drop_list');
    drupal_add_js(array('fullcalendar_drop_list' => $settings), 'setting');
  }

  function defaultable_sections(&$sections, $section = NULL) {
    if ($section == 'fullcalendar_drop_list') {
      $sections[$section][] = $section;
    }
  }

  /**
   * Wrapper around views_plugin_display::get_field_labels().
   *
   * @param array|string|null $type
   *   Either a string or array of field types.
   *
   * @return array
   *   An associative array of field labels, keyed by their field id.
   */
  function fullcalendar_drop_list_fields($type = NULL) {
    $this->view->init_handlers();
    $labels = $this->display->get_field_labels();
    // If specific field types are needed, restrict the options.
    if (isset($type)) {
      // Allow both single and multiple types.
      if (!is_array($type)) {
        $type = array($type);
      }
      foreach ($this->view->field as $id => $field) {
        if (!isset($field->field_info) || !in_array($field->field_info['type'], $type)) {
          unset($labels[$id]);
        }
        else {
          $entity_type = $field->definition['entity_tables'][$field->base_table];
        }
      }
    }
    return $labels;
  }
}
