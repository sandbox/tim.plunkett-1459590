<?php

/**
 * @file
 * @todo.
 */

/**
 * Implements hook_fullcalendar_options_info().
 */
function fullcalendar_drop_list_fullcalendar_options_info() {
  return array(
    'fullcalendar_drop_list' => array(
      'name' => t('FullCalendar Drop List'),
      'js' => TRUE,
      'no_fieldset' => TRUE,
    ),
  );
}
