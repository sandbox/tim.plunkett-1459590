<?php

/**
 * @file
 * @todo.
 */

/**
 * Implements hook_views_plugins().
 */
function fullcalendar_drop_list_views_plugins() {
  $plugins = array();

  $plugins['display_extender']['fullcalendar_drop_list'] = array(
    'title' => t('FullCalendar Drop List'),
    'help' => t('Todo'),
    'handler' => 'fullcalendar_drop_list_plugin_display_extender',
    'path' => drupal_get_path('module', 'fullcalendar_drop_list') . '/plugins',
  );

  return $plugins;
}
