/**
 * @file
 * @todo.
 */

(function ($) {

Drupal.behaviors.fullcalendar_drop_list = {
  attach: function (context, settings) {
    var settings = settings.fullcalendar_drop_list;

    for (var dom_id in settings.dom_ids) {
      if (!settings.dom_ids.hasOwnProperty(dom_id)) {
        return;
      }

      $(context).find('.' + settings.dom_ids[dom_id] + ' .fullcalendar-drop-list').each(function () {
        var eventObject = {
          title: $.trim($(this).text()),
          className: 'fc-event-default',
          field: settings.field,
          entity_type: settings.entity_type,
          entity_id: $(this).attr('eid'),
          url: '#'
        }
        $(this).closest('.views-field').parent()
          .data('eventObject', eventObject)
          .draggable({
            zIndex: 9999,
            revert: true,
            revertDuration: 0
          });
      });
    }
  }
};

}(jQuery));
