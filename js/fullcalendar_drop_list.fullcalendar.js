/**
 * @file
 * @todo.
 */

(function ($) {

Drupal.fullcalendar.plugins.fullcalendar_drop_list = {
  drop: function (date, allDay, jsEvent, ui, object, fullcalendar) {
    var eventObject = $.extend({start: date, allDay: allDay ? 1 : 0}, $(object).data('eventObject'));

    var element_settings = {
      event: 'fullcalendar_drop_list',
      url: 'fullcalendar_drop_list/ajax/update/' + eventObject.entity_type + '/' + eventObject.entity_id,
      submit: {field: eventObject.field, date: date.getTime()}
    };

    var ajax = new Drupal.ajax(fullcalendar.$calendar, object, element_settings);
    $(ajax.element).trigger('fullcalendar_drop_list');

    fullcalendar.$calendar.find('.fullcalendar').fullCalendar('renderEvent', eventObject, true);
    if (Drupal.settings.fullcalendar_drop_list) {
      $(object).remove();
    }
  }
};

}(jQuery));
